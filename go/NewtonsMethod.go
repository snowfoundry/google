package main

import (
	"fmt"
	"math"
)

type ErrNegativeSqrt float64
type ErrUnknown float64

func (e ErrUnknown) Error() string {
	return fmt.Sprintln("could not approximate a sqrt value for ", float64(e))
}

func (e ErrNegativeSqrt) Error() string {
	return fmt.Sprintln("cannot Sqrt negative number: ", float64(e))
}

func Sqrt(x float64) (float64, error) {
	threshold := float64(0.0000001)

	if x < 0 {
		return -1, ErrNegativeSqrt(x)
	}

	for last := x / 2; true; {
		z := last - ((last*last)-x)/(2*last)
		if math.Abs(z-last) < threshold {
			return z, nil
		}

		last = z
	}
	return -1, ErrUnknown(x)
}

func main() {
	fmt.Println(Sqrt(-2))
	fmt.Println(Sqrt(2))
	fmt.Println(math.Sqrt(2))
	fmt.Println(Sqrt(3))
	fmt.Println(math.Sqrt(3))
	fmt.Println(Sqrt(4))
	fmt.Println(math.Sqrt(4))
	fmt.Println(Sqrt(5))
	fmt.Println(math.Sqrt(5))
	fmt.Println(Sqrt(100))
	fmt.Println(math.Sqrt(100))
}
