package main
// Quick find implementation from Algorithms by Sedgewick / Wayne

import (
	"fmt"
)

type List struct {
	xs 		[]int
	sz 		[]int
	length 	int
}

func NewList(length int) *List {
	xs := List{ make([]int, length), make([]int, length), length }
	for i := 0; i < length; i++ {
		xs.xs[i] = i
		xs.sz[i] = 1
	}
	return &xs
}

func (xs *List) connected(p, q int) bool {
	return xs.root(p) == xs.root(q)
}

func (xs *List) root(p int) int {
	for p != xs.xs[p] {
		xs.xs[p] = xs.xs[xs.xs[p]]
		p = xs.xs[p]
	}

	return p
}

func (xs *List) union(p, q int) {
	fmt.Printf("union %d -> %d\n", p, q)
	pid := xs.root(p)
	qid := xs.root(q)

	if xs.sz[pid] < xs.sz[qid] {
		xs.xs[pid] = qid
		xs.sz[qid] += xs.sz[pid]
	} else {
		xs.xs[qid] = pid
		xs.sz[pid] += xs.sz[qid]
	}
}

func (xs *List) print() {
	for i := range xs.xs {
		if i > 0 {
			fmt.Printf(", ")
		}

		if i % 10 == 0 && i > 0 {
			fmt.Printf("\n")
		}

		fmt.Printf("[%2d]:%3d", i, xs.xs[i])
	}
	fmt.Println()
}

func main() {
	xs := NewList(10)
	xs.print()
	xs.union(2,4)
	xs.union(5,4)
	xs.union(2,9)
	xs.print()
}