LANGUAGES=go c

.PHONY: all $(LANGUAGES)
all: $(LANGUAGES)

go:
	cd go; make; cd ..
c:
	cd c; make; cd ..

clean:
	rm -f bin/c/*
	rm -f bin/go/*
