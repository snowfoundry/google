#include <stdio.h>
#include <pwd.h>
#include <errno.h>

void output(char* username, int uid, int gid, char* dir, char* shell) {
	printf("username=%s uid=%d gid=%d home=%s shell=%s\n",
			username,
			uid,
			gid,
			dir,
			shell);
}

int main(int argc, char** argv) {
	if (argc != 2) {
		printf("Usage: id username\n");
		return 255;
	}

	struct passwd *data = getpwnam(argv[1]);
	
	if (data != NULL) {
		output(data->pw_name, data->pw_uid, data->pw_gid, data->pw_dir, data->pw_shell);
	} else {
		if (errno == 0) {
			printf("Username was not found\n");
		} else {
			printf("Could not retrieve information on user %s\n", argv[1]);
			return 254;
		}
	}

	return 0;
}
