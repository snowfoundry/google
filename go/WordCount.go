package main

import (
	"code.google.com/p/go-tour/wc"
	"fmt"
	"strings"
)

func WordCount(s string) map[string]int {
	result := make(map[string]int)

	for _, v := range strings.Fields(s) {
		result[v] = result[v] + 1
	}

	return result
}

func main() {
	wc.Test(WordCount)
	fmt.Println()
}
