package main
;; This was edited
import (
	"io"
	"os"
)

func main() {
	var input *os.File
	var err error

	if len(os.Args) == 2 {
		input, err = os.Open(os.Args[1])
		if err != nil {
			panic(err)
		}
		defer input.Close()
	} else {
		input = os.Stdin
	}

	_, err = io.Copy(os.Stdout, input)

	if err != nil {
		panic(err)
	}

}
