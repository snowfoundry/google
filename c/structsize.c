#include <stdio.h>

struct justchar {
	char x;
};

struct charpointer {
	char *x;
};

struct charpointerwithchar {
	char *x;
	char y;
};

struct withint {
	char x;
	int y;
};

int main(int argc, char** argv) {
	struct justchar x;
	struct withint y;
	struct charpointer z;
	struct charpointerwithchar a;

	printf("Sizeof char: %lu\n", sizeof(char));
	printf("Sizeof int: %lu\n", sizeof(int));
	printf("Sizeof char*: %lu\n", sizeof(char*));
	printf("Sizeof struct with char: %lu\n", sizeof(x));

	// This should include an extra 3 bytes for padding
	printf("Sizeof struct with char and int: %lu\n", sizeof(y));
	printf("Sizeof struct with char*: %lu\n", sizeof(z));
	printf("Sizeof struct with char* and char: %lu\n", sizeof(a));
	return 0;
}
