package main

import (
	"fmt"
)

func BinarySearch(xs []int, key int) int {
	lo := 0
	hi := len(xs) - 1

	for lo <= hi {
		mid := lo + (hi - lo) / 2
		switch {
			case key < xs[mid]: hi = mid - 1
			case key > xs[mid]: lo = mid + 1
			default: return mid
		}
	}
	return -1
}

func main() {
	x := []int{1,2,3,4,5}
	fmt.Println(BinarySearch(x, 9))	
	fmt.Println(BinarySearch(x, 2))	
	fmt.Println(BinarySearch(x, 3))	
	fmt.Println(BinarySearch(x, 1111))	
	fmt.Println(BinarySearch(x, 0))	
}