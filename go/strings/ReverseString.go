package strings

func Reverse(s string) string {
	b := []byte(s)
	for i := 0; i < len(b) / 2; i++ {
		j := len(b) - i - 1
		b[i], b[j] = b[j], b[i]
	}
	return string(b)
}

func ReverseWords(s string) string {
	reversed := Reverse(s)
	b := []byte(reversed)
	lastSpace := 0

	for i := 0; i < len(b); i++ {
		if b[i] == ' ' {
			if lastSpace == (i - 1) {
				lastSpace = i
				continue
			}
			for j := lastSpace; j < i; j++ {
				k := (i - lastSpace) - j - 1
				b[j], b[k] = b[k], b[j]
			}
			lastSpace = i
		}
	}
	return string(b)
}
