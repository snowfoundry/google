#include <stdio.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <string.h>

void usage();

int main(int argc, char **argv)
{
	char buffer[1024];
	int outputfd;

	if(argc != 2) {
		usage();
		return 1;
	}

	outputfd = open(argv[1], O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if(outputfd == -1) {
		perror("Could not open file for writing");
		return 1;
	}

	while(fgets(buffer, 1024, stdin))
	{
		printf("%s", buffer);
		if(write(outputfd, buffer, strlen(buffer)) == -1)
		{
			perror("Could not write to file!");
			return 1;
		}
		fsync();
	}


	return 0;
}

void usage() {
	printf("tee filename\n");
}
