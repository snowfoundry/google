package main

import (
	"fmt"
	"os"
	"bufio"
	"math/rand"
	"math"
	"strconv"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Usage: DataSetGenerator output_file number_of_lines")
		os.Exit(1)
	}

	iterations, err := strconv.ParseInt(os.Args[2], 10, 8)
	if err != nil {
		fmt.Println("Unable to parse number_of_lines")
		os.Exit(2)
	}

	output, err := os.Create(os.Args[1])
	if err != nil { panic(err) }
	defer output.Close()
	output_buffer := bufio.NewWriter(output)

	for i := 0; i < int(iterations); i++ {
		if _, err := output_buffer.Write([]byte(fmt.Sprintf("%d %d\n", rand.Intn(int(math.Pow(2,8))), rand.Intn(int(math.Pow(2,8)))))); err != nil {
			panic(err)
		}
	}

	if err = output_buffer.Flush(); err != nil {panic(err)}
}