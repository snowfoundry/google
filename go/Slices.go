package main

import "code.google.com/p/go-tour/pic"

// Task: Implement Pic.  It should return a slice of length dy, each element of
// which is a slice of dx 8-bit unsigned integers.
func Pic(dx, dy int) [][]uint8 {
	picture := make([][]uint8, dy)
	for i, _ := range picture {
		picture[i] = make([]uint8, dx)
		for j, _ := range picture[i] {
			picture[i][j] = uint8((i + j) * (j - i))
		}
	}
	return picture
}

func main() {
	pic.Show(Pic)
}
