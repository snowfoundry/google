package main

import (
	"fmt"
)

func InsertionSort(xsUnsorted []int) []int {
	for i := 0; i < len(xsUnsorted); i++ {
		for j := i; j > 0 && xsUnsorted[j] < xsUnsorted[j-1]; j-- {
			xsUnsorted[j], xsUnsorted[j-1] = xsUnsorted[j-1], xsUnsorted[j]
		}
	}
	return xsUnsorted
}

func main() {
	fmt.Println(InsertionSort([]int{1, 3, 2}))
}
