package main

import "fmt"
import "os"

func main() {
	if len(os.Args) != 2 {
		usage()
	} else {
		fmt.Println(intToHex(0))
		fmt.Println(intToHex(11))
		fmt.Println(intToHex(14))
		fmt.Println(intToHex(16))
		fmt.Println(intToHex(17))
		fmt.Println(intToHex(32))
		fmt.Println(intToHex(49))
	}
	return
}

func usage() {
	fmt.Println("IntToHex integer")
}

func intToHex(n int) string {
	const base16 string = "0123456789ABCDEF"
	result := ""

	if n == 0 {
		result = "0"
	}

	for n > 0 {
		//fmt.Printf("loop: %d\n", n)
		result = string(base16[n%16]) + result
		n = n >> 4
	}

	return "0x" + result
}
