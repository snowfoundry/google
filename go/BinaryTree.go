package main

import (
	"fmt"
)

type ErrValueNotFound float64

func (e ErrValueNotFound) Error() string {
	return fmt.Sprint("Value not found in tree: ", float64(e))
}

type BinaryTree struct {
	root *Node
}

type Node struct {
	value       int
	left, right *Node
	cnt         uint
}

func NewBinaryTree(value int) *BinaryTree {
	return &BinaryTree{NewNode(value)}
}

func NewNode(value int) *Node {
	return &Node{value, nil, nil, 1}
}

func (b *BinaryTree) Insert(value int) {
	if b.root == nil {
		b.root = NewNode(value)
	} else {
		b.root.Insert(&value)
	}
}

func (n *Node) Insert(value *int) {
	switch {
	case *value < n.value && n.left == nil:
		n.left = NewNode(*value)
	case *value > n.value && n.right == nil:
		n.right = NewNode(*value)
	case *value < n.value:
		n.left.Insert(value)
	case *value > n.value:
		n.right.Insert(value)
	}
}

func (b *BinaryTree) Get(value int) (int, error) {
	if b.root.value == value {
		return b.root.value, nil
	} else {
		return b.root.Get(value)
	}

	return 0, ErrValueNotFound(value)
}

func (n *Node) Get(value int) (int, error) {
	if n.value == value {
		return n.value, nil
	} else {
		if value < n.value && n.left != nil {
			return n.left.Get(value)
		} else if value > n.value && n.right != nil {
			return n.right.Get(value)
		}
	}

	return 0, ErrValueNotFound(value)
}

func main() {
	fmt.Println("Fantastic binary tree test")
	var bTree *BinaryTree = NewBinaryTree(5)
	bTree.Insert(99)
	fmt.Println(bTree.Get(5))
	fmt.Println(bTree.Get(6))
	fmt.Println(bTree.Get(99))
}
