package strings

import (
	"testing"
)

func TestRacecar(t *testing.T) {
	if Reverse("racecar") != "racecar" {
		t.Error("racecar != racecar")
	}
}

func TestTest(t *testing.T) {
	if Reverse("test") != "tset" {
		t.Error("test != tset")
	}
}
