package main
// Quick find implementation from Algorithms by Sedgewick / Wayne

import (
	"fmt"
)

type List struct {
	xs []int
	length int
}

func NewList(length int) *List {
	xs := List{ make([]int, length), length }
	for i := 0; i < length; i++ {
		xs.xs[i] = i
	}
	return &xs
}

func (xs *List) connected(p, q int) bool {
	return xs.xs[p] == xs.xs[q]
}

func (xs *List) union(p, q int) {
	fmt.Printf("union %d -> %d\n", p, q)
	pid := xs.xs[p]
	qid := xs.xs[q]
	for i := 0; i < xs.length; i++ {
		if xs.xs[i] == pid {
			xs.xs[i] = qid
		}
	}
}

func (xs *List) print() {
	for i := range xs.xs {
		if i > 0 {
			fmt.Printf(", ")
		}

		if i % 10 == 0 && i > 0 {
			fmt.Printf("\n")
		}

		fmt.Printf("[%2d]:%3d", i, xs.xs[i])
	}
	fmt.Println()
}

func main() {
	xs := NewList(10)
	xs.print()
	xs.union(2,4)
	xs.union(5,4)
	xs.union(2,9)
	xs.print()
}