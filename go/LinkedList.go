package main

import (
	"bytes"
	"fmt"
	"log"
	//	"test"
)

// Singely linked list
type LinkedList struct {
	head *Element
}

// Element is contained by a LinkedList
type Element struct {
	value interface{}
	next  *Element
}

// Add a value to the front of the linked list
func (l *LinkedList) addFront(value interface{}) {
	if l.head == nil {
		l.head = &Element{value, nil}
	} else {
		l.head = &Element{value, l.head}
	}
}

// Add a value to the end of the linked list
func (l *LinkedList) addBack(value interface{}) {
	if l.head == nil {
		l.head = &Element{value, nil}
	} else {
		elem := l.head

		for elem.next != nil {
			elem = elem.next
		}

		elem.next = &Element{value, nil}
	}
}

func (l *LinkedList) String() string {
	var buffer bytes.Buffer
	head := l.head

	if head == nil {
		return "Empty"
	} else {
		switch head.value.(type) {
		case string:
			buffer.WriteString(head.value.(string))
		case int:
			buffer.WriteString(fmt.Sprintf("%d", head.value.(int)))
		default:
			buffer.WriteString("Unknown")
		}

		for head.next != nil {
			head = head.next

			switch head.value.(type) {
			case string:
				buffer.WriteString(" " + head.value.(string))
			case int:
				buffer.WriteString(fmt.Sprintf(" %d", head.value.(int)))
			default:
				buffer.WriteString("Unknown")
			}
		}
	}

	return buffer.String()
}
func main() {
	log.Println("Initializing linked list")
	ll := new(LinkedList)
	ll.addBack("this breaks it")
	ll.addFront(1)
	ll.addBack("testing")
	ll.addFront(2239231)
	fmt.Println(ll)
}
