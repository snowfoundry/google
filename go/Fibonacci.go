package main

import "fmt"

// Task: Implement a fibonacci function that returns a function (a closure)
// that returns successive fibonacci numbers
func fibonacci() func() int {
	var start int = 0
	var cnt int = 0
	var temp int = 0

	return func() int {
		if start == 0 {
			start = 1
		} else {
			temp = cnt + start
			start = cnt
			cnt = temp
		}

		return cnt
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}
